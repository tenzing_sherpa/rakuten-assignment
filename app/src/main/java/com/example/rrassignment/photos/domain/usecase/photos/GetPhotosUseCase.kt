package com.example.rrassignment.photos.domain.usecase.photos

import com.example.rrassignment.photos.data.mapper.photos.PhotosToDbMapper
import com.example.rrassignment.photos.domain.model.PhotosData
import com.example.rrassignment.photos.domain.repository.PhotosRepository
import com.example.rrassignment.photos.domain.usecase.photos.GetPhotosUseCase.Result.Success
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
/**
 * UseCase with repository to complete the background tasks
 * @param photosRepository repository to handle different tasks
 * */
class GetPhotosUseCase @Inject constructor(private val photosRepository: PhotosRepository) {

    /**
     * Seal class to handle the state of the background call for fetching
     * data to update the UI
     * */
    sealed class Result {
        object Loading : Result()
        data class Success(val response: List<PhotosData>) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    /**
     * function to observe the data from sources checking network availability
     * @param hasNetwork boolean value of network availability
     * */
    fun getPhotos(hasNetwork: Boolean): Observable<Result> {
        return if (!hasNetwork) {
            return photosRepository.getPhotosFromDb()
                .toObservable()
                .map {
                    Success(it) as Result
                }
                .onErrorReturn { Result.Failure(it) }
                .startWith(Result.Loading)
        } else {
            photosRepository.deleteAllPhotos() // delete all data to update new datas
            photosRepository.getPhotos().toObservable().map {
                val data = PhotosToDbMapper().reverseMap(it.photos.photo)
                Success(data) as Result
            }
                .onErrorReturn { Result.Failure(it) }
                .startWith(Result.Loading)
        }
    }
}