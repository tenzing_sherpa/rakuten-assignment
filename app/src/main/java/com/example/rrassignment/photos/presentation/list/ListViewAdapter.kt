package com.example.rrassignment.photos.presentation.list

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.example.rrassignment.R
import com.example.rrassignment.core.ext.Utils.getMagicPhotoUrlLink
import com.example.rrassignment.photos.domain.model.PhotosData

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
class ListViewAdapter(private val mContext: Context, private val photoList: ArrayList<PhotosData>) :
    BaseAdapter() {
    private lateinit var titleTextView: TextView
    private lateinit var imageView: AppCompatImageView

    override fun getCount(): Int {
        return photoList.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val convertView =
            LayoutInflater.from(mContext).inflate(R.layout.list_single_item, parent, false)

        val photoData = photoList[position]
        titleTextView = convertView.findViewById(R.id.txt_title)
        imageView = convertView.findViewById(R.id.image_view)
        titleTextView.text = photoData.title

//        get image url link passing different data
        val photoUrlLink = getMagicPhotoUrlLink(
            serverId = photoData.server.toString(),
            photoId = photoData.id.toString(),
            secretId = photoData.secret.toString()
        )
//        Glide library to show image to AppCompactImageView
        Glide.with(mContext)
            .load(photoUrlLink)
            .into(imageView)
        return convertView
    }


}

