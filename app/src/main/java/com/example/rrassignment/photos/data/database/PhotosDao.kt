package com.example.rrassignment.photos.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.rrassignment.photos.domain.model.PhotosData
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */

@Dao
interface PhotosDao {

    // insert all photos in the table
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(characters: List<PhotosData>): Maybe<List<Long>>

//    fetch all data of photos table from local database
    @Query("Select * From Photos")
    fun getAllPhotos(): Maybe<List<PhotosData>?>

//    delete all data from photos table
    @Query("DELETE FROM Photos")
    fun deleteAllPhotos(): Single<Int>

}