package com.example.rrassignment.photos.presentation.details

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.rrassignment.core.ext.Utils
import com.example.rrassignment.databinding.ActivityPhotoDetailsBinding
import com.example.rrassignment.photos.domain.model.PhotosData
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhotoDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPhotoDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPhotoDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

       // showing the back button in action bar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val data = intent.getParcelableExtra<PhotosData>("photo_data")

        val serverId = data?.server
        val photoId = data?.id
        val secretId = data?.secret

        val photoUrlLink = Utils.getMagicPhotoUrlLink(
            serverId = serverId.toString(),
            photoId = photoId.toString(),
            secretId = secretId.toString()

        )
        Glide.with(this)
            .load(photoUrlLink)
            .into(binding.detailsImageView)

        binding.txtPhotoId.text = StringBuilder().append("ID: ").append(photoId)
        binding.txtPhotoOwner.text = StringBuilder().append("OWNER: ").append(data?.owner)
        binding.txtPhotoSecret.text = StringBuilder().append("SECRET: ").append(secretId)
        binding.txtPhotoServer.text = StringBuilder().append("SERVER: ").append(serverId)
        binding.txtPhotoFarm.text = StringBuilder().append("FARM: ").append(data?.farm)
        binding.txtPhotoTitle.text = StringBuilder().append("TITLE: ").append(data?.title)
        binding.txtPhotoIsPublic.text = StringBuilder().append("IS_PUBLIC: ").append(data?.ispublic)
        binding.txtPhotoIsFriend.text = StringBuilder().append("IS_FRIEND: ").append(data?.isFriend)
        binding.txtPhotoIsFamily.text = StringBuilder().append("IS_FAMILY: ").append(data?.isFamily)
    }
    // enable the back
    // function to the button on press
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}