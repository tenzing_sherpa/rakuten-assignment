package com.example.rrassignment.photos.domain.usecase.photos

import com.example.rrassignment.photos.domain.model.PhotosData
import com.example.rrassignment.photos.domain.repository.PhotosRepository
import io.reactivex.Maybe
import javax.inject.Inject

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
class InsertPhotosUseCase @Inject constructor(private val  photosRepository: PhotosRepository) {

 fun insertPhotos(data: List<PhotosData>): Maybe<List<Long>> {
  return photosRepository.insertAllPhotos(data)
 }
}