package com.example.rrassignment.photos.data.mapper.photos

import com.example.rrassignment.core.database.Mapper
import com.example.rrassignment.photos.data.response.ResultItem
import com.example.rrassignment.photos.domain.model.PhotosData

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
class PhotosToDbMapper : Mapper<PhotosData, ResultItem>() {
    override fun map(value: PhotosData?): ResultItem {
        throw UnsupportedOperationException()
    }

    override fun reverseMap(value: ResultItem?): PhotosData? {
        return if (value == null) {
            null
        } else {
         PhotosData(
             value.uuid,
                value.id,
                value.farm,
                value.isFamily,
                value.isFriend,
                value.ispublic,
                value.owner,
                value.secret,
                value.server,
                value.title
            )
        }
    }
}