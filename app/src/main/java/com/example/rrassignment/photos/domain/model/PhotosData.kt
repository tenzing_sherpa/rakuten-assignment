package com.example.rrassignment.photos.domain.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.parcelize.Parcelize
import java.util.*

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */

@Parcelize
@Entity(tableName = "Photos")
data class PhotosData(
  @PrimaryKey(autoGenerate = true)
  val uuid: Int = 0,
  val id: String? = null,
  val farm: Int? = 0,
  val isFamily: Int? = 0,
  val isFriend: Int? = 0,
  val ispublic: Int? = 0,
  val owner: String? = null,
  val secret: String? = null,
  val server: String? = null,
  val title: String? = null
):  Parcelable