package com.example.rrassignment.photos.domain.repository

import com.example.rrassignment.photos.data.response.ResponsePhotos
import com.example.rrassignment.photos.domain.model.PhotosData
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
interface PhotosRepository {

 fun getPhotosFromDb(): Maybe<List<PhotosData>?> // get list of photos from local database
 fun getPhotos(): Single<ResponsePhotos> // get list of photos from remote server
 fun insertAllPhotos(data: List<PhotosData>):Maybe<List<Long>> // insert all photos in local database
 fun deleteAllPhotos():Single<Int> // delete all photos from local database

}