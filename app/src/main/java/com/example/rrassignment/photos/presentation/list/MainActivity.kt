package com.example.rrassignment.photos.presentation.list

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.example.rrassignment.R
import com.example.rrassignment.core.lib.SimpleQuickReturn
import com.example.rrassignment.core.network.NetworkConnection
import com.example.rrassignment.photos.domain.model.PhotosData
import com.example.rrassignment.photos.presentation.PhotosViewModel
import com.example.rrassignment.photos.presentation.details.PhotoDetailsActivity
import com.example.rrassignment.photos.presentation.list.ListViewAdapter
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable


/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val photoViewModel : PhotosViewModel by viewModels()
    private val disposables = CompositeDisposable()
    private lateinit var listView: ListView
    private  lateinit var photosListAdapter : ListViewAdapter
    private var photoList = ArrayList<PhotosData>()
    private lateinit var photosData: PhotosData
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //implementing Quick Return pattern
        //set the content view with header or footer or both to create a whole view
        val view = SimpleQuickReturn(applicationContext)
            .setContent(R.layout.activity_main)
            .setFooter(R.layout.footer)
            .createView()
        setContentView(view)

        listView = findViewById(R.id.list_view)
        progressBar = findViewById(R.id.progress_circular)

        initAdapter() // initialize the adapter
        photoViewModel.bound(NetworkConnection.checkNetwork())
        observeData() //observe the network data response

        listView.setOnItemClickListener { parent, view, position, id ->

            photosData = photoList[position]
            val intent = Intent(this,PhotoDetailsActivity::class.java)
            intent.putExtra("photo_data",photosData)
            startActivity(intent)
        }

    }

    /**
     * initialize the listview adapter
     * set adapter to listview
     * */
    private fun initAdapter(){
        photosListAdapter = ListViewAdapter(this,photoList)
        listView.adapter = photosListAdapter

    }

    /**
     * function to observe the response data from local/remote server
     * notify the data set changes to listview adapter
     * */
    private fun observeData(){
        photoViewModel.charsList.observe(this, Observer {
            photoList.addAll(it)
            photosListAdapter.notifyDataSetChanged()
            if (photoViewModel.progressVisible.value == true)
                progressBar.isVisible = false
        })
    }
    /**
     * check whether the photosData object is initialize or not
     * set latest visited photosData object title to footer title TextView
     * */
    override fun onResume() {
        super.onResume()

        if(::photosData.isInitialized) {
            val footerTitleTextView = findViewById<TextView>(R.id.footer_textView)
            footerTitleTextView.text = photosData.title
        }
    }

    override fun onPause() {
        disposables.clear()
        super.onPause()
    }

    override fun onDestroy() {
        photoViewModel.unbound()
        super.onDestroy()
    }


}