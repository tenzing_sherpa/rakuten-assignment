package com.example.rrassignment.photos.presentation

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.rrassignment.core.ext.addTo
import com.example.rrassignment.photos.domain.model.PhotosData
import com.example.rrassignment.photos.domain.usecase.photos.GetPhotosUseCase
import com.example.rrassignment.photos.domain.usecase.photos.InsertPhotosUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */

/**
 * PhotosViewModel to handle the different background task using different usecases
 * @param getPhotosUseCase useCase to get all the photos
 * @param insertPhotosUseCase useCase to insert data to local database
 * */
@HiltViewModel
class PhotosViewModel @Inject constructor(
    private var getPhotosUseCase: GetPhotosUseCase,
    private var insertPhotosUseCase: InsertPhotosUseCase
) : ViewModel() {

    private val disposables = CompositeDisposable()
    val progressVisible = MutableLiveData<Boolean>()
    val charsList = MutableLiveData<List<PhotosData>>()

    /**
     * function to get all the photos
     * @param hasNetwork boolean value whether network is available or not
     * */
    fun bound(hasNetwork: Boolean) {
        getPhotosUseCase.getPhotos(hasNetwork)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleResult(it) }
            .addTo(disposables)

    }

    /**
     * function to handle the result of the GetPhotosUseCase
     * @param result seal class of Result to handle the state
     * */
    private fun handleResult(result: GetPhotosUseCase.Result) {
        when (result) {
            is GetPhotosUseCase.Result.Loading -> progressVisible.value = true
            is GetPhotosUseCase.Result.Success -> {
                insert(result.response)
                charsList.value = result.response
                progressVisible.value = false
            }
            is GetPhotosUseCase.Result.Failure -> "error"
        }
    }

    /**
     * function to insert all photos data to local database
     * @param data list of photosData object
     * */
    @SuppressLint("CheckResult")
    private fun insert(data: List<PhotosData>) {
        insertPhotosUseCase.insertPhotos(data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    /**
     * function to clear disposables
     * */
    fun unbound() {
        disposables.clear()
    }


}