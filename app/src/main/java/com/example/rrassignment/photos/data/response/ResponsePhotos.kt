package com.example.rrassignment.photos.data.response

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.parcelize.Parcelize


@Parcelize
data class ResponsePhotos(
    @field:JsonProperty("photos")
    val photos: Photos,
    @field:JsonProperty("stat")
    val stat: String
) : Parcelable


@Parcelize
data class Photos(
    @field:JsonProperty("page")
    val page: Int,
    @field:JsonProperty("pages")
    val pages: Int,
    @field:JsonProperty("perpage")
    val perPage: Int,
    @field:JsonProperty("photo")
    val photo: List<ResultItem>,
    @field:JsonProperty("total")
    val total: Int
) : Parcelable


@Parcelize
data class ResultItem(
    val uuid: Int,
    @field:JsonProperty("farm")
    val farm: Int,
    @field:JsonProperty("id")
    val id: String,
    @field:JsonProperty("isfamily")
    val isFamily: Int,
    @field:JsonProperty("isfriend")
    val isFriend: Int,
    @field:JsonProperty("ispublic")
    val ispublic: Int,
    @field:JsonProperty("owner")
    val owner: String,
    @field:JsonProperty("secret")
    val secret: String,
    @field:JsonProperty("server")
    val server: String,
    @field:JsonProperty("title")
    val title: String
) : Parcelable