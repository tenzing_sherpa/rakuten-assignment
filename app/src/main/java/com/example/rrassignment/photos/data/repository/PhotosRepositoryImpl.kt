package com.example.rrassignment.photos.data.repository

import com.example.rrassignment.photos.data.api.PhotosApiImpl
import com.example.rrassignment.photos.data.database.PhotosDao
import com.example.rrassignment.photos.data.response.ResponsePhotos
import com.example.rrassignment.photos.domain.model.PhotosData
import com.example.rrassignment.photos.domain.repository.PhotosRepository
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
/**
 * Repository to provide data from local and remote source
 * @param photoRemoteSource remote data source for api call
 * @param photoLocalSource local data source
 * */
class PhotosRepositoryImpl @Inject constructor(
    private val photoRemoteSource: PhotosApiImpl,
    private val photoLocalSource: PhotosDao
) : PhotosRepository {
    override fun getPhotosFromDb(): Maybe<List<PhotosData>?> {
        return photoLocalSource.getAllPhotos()
    }

    override fun getPhotos(): Single<ResponsePhotos> {
        return photoRemoteSource.getImagesList()
    }

    override fun insertAllPhotos(data: List<PhotosData>): Maybe<List<Long>> {
        return photoLocalSource.insertPhotos(data)
    }

    override fun deleteAllPhotos(): Single<Int> {
        return photoLocalSource.deleteAllPhotos()
    }

}