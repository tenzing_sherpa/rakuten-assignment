package com.example.rrassignment.photos.data.api

import com.example.rrassignment.photos.data.response.ResponsePhotos
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
class PhotosApiImpl
@Inject constructor(private val apiInterface: PhotosApi) {
    fun getImagesList(): Single<ResponsePhotos> {
        return apiInterface.getImagesList()
    }
}