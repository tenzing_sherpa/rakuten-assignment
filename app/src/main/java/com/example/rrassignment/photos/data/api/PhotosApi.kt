package com.example.rrassignment.photos.data.api

import com.example.rrassignment.photos.data.response.ResponsePhotos
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
interface PhotosApi {

    @GET("photos.json?method=flickr.photos.getRecent&amp;api_key=fee10de350d1f31d5fec0eaf330d2dba&amp;page=1&amp;format=json&amp;nojsoncallback=true&amp;safe_search=true")
    fun getImagesList(): Single<ResponsePhotos>
}