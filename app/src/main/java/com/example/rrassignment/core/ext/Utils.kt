package com.example.rrassignment.core.ext

/**
 * Created by TENZING SHERPA on 4/3/22.
 * Email: tenzingherpaaa@gmail.com
 */
object Utils {

    /**
     * function to get the image url link using string interpolation
     * @param serverId server ID
     * @param photoId photo ID
     * @param secretId secret key
     * */
    fun getMagicPhotoUrlLink(serverId: String, photoId: String, secretId: String): String {
        return "https://farm6.staticflickr.com/${serverId}/${photoId}_${secretId}.jpg"
    }
}