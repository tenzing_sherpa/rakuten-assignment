package com.example.rrassignment.core.repository

import com.example.rrassignment.photos.data.repository.PhotosRepositoryImpl
import com.example.rrassignment.photos.domain.repository.PhotosRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    /**
     * provider for building the repository for PhotosRepository
     * @param repo PhotosRepositoryImpl
     * */
    @Provides
    fun provideImageListRepository(repo: PhotosRepositoryImpl): PhotosRepository = repo

}