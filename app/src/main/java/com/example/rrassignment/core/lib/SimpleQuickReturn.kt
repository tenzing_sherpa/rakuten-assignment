package com.example.rrassignment.core.lib

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.AbsListView
import android.widget.FrameLayout
import android.widget.ListView
import com.example.rrassignment.R
import com.example.rrassignment.core.lib.ListViewScrollObserver.OnListViewScrollListener

/**
 * Created by TENZING SHERPA on 4/3/22.
 * Email: tenzingherpaaa@gmail.com
 */
class SimpleQuickReturn(private val mContext: Context) {
    private var mAnimation: Animation? = null
    private var mIsSnapped = true
    private var mIsWaitingForExactHeaderHeight = true
    private var mIsScrollingUp // True if the last scroll movement was in the "up" direction.
            = false
    private var mIsUsingHeader = false
    private var mIsUsingFooter = false
    private var mContentResId = 0
    private var mHeaderResId = 0
    private var mFooterResId = 0
    private var mHeaderHeight = 0
    private var mFooterHeight = 0
    private var mHeaderTop = 0
    private var mFooterBottom = 0
    private lateinit var mInflater: LayoutInflater
    private var mRealHeaderLayoutParams: FrameLayout.LayoutParams? = null
    private var mRealFooterLayoutParams: FrameLayout.LayoutParams? = null
    private lateinit var mListView: ListView
    private var mOnSnappedChangeListener: OnSnappedChangeListener? = null
    private lateinit var mContent: View
    private var mDummyHeader: View? = null
    private var mDummyFooter: View? = null
    private var mRealHeader: View? = null
    private var mRealFooter: View? = null
    private var mRoot: ViewGroup? = null

    interface OnSnappedChangeListener {
        fun onSnappedChange(snapped: Boolean)
    }

    fun setOnSnappedChangeListener(onSnapListener: OnSnappedChangeListener?) {
        mOnSnappedChangeListener = onSnapListener
    }

    fun setContent(contentResId: Int): SimpleQuickReturn {
        mContentResId = contentResId
        return this
    }

    fun setHeader(headerResId: Int): SimpleQuickReturn {
        mHeaderResId = headerResId
        mIsUsingHeader = true
        return this
    }

    fun setFooter(footerResId: Int): SimpleQuickReturn {
        mFooterResId = footerResId
        mIsUsingFooter = true
        return this
    }

    @SuppressLint("Range")
    fun createView(): View? {
        mInflater = LayoutInflater.from(mContext)
        mContent = mInflater.inflate(mContentResId, null)
        if (mIsUsingHeader) {
            mRealHeader = mInflater.inflate(mHeaderResId, null)
            mRealHeaderLayoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
            mRealHeaderLayoutParams!!.gravity = Gravity.TOP
        }
        if (mIsUsingFooter) {
            mRealFooter = mInflater.inflate(mFooterResId, null)
            mRealFooterLayoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
            mRealFooterLayoutParams!!.gravity = Gravity.BOTTOM
        }

        // Use measured height here as an estimate of the header height
        // later on after the layout is complete we'll use the actual height
        var widthMeasureSpec = 0
        var heightMeasureSpec = 0
        // http://stackoverflow.com/a/20147939
        widthMeasureSpec =
            MeasureSpec.makeMeasureSpec(FrameLayout.LayoutParams.MATCH_PARENT, MeasureSpec.AT_MOST)
        heightMeasureSpec =
            MeasureSpec.makeMeasureSpec(FrameLayout.LayoutParams.WRAP_CONTENT, MeasureSpec.AT_MOST)
        if (mIsUsingHeader) {
            mRealHeader!!.measure(widthMeasureSpec, heightMeasureSpec)
            mHeaderHeight = mRealHeader!!.measuredHeight
        }
        if (mIsUsingFooter) {
            mRealFooter!!.measure(widthMeasureSpec, heightMeasureSpec)
            mFooterHeight = mRealFooter!!.measuredHeight
        }
        mListView = mContent.findViewById<View>(R.id.list_view) as ListView
        createListView()
        return mRoot
    }

    private fun createListView() {
        mRoot = mInflater.inflate(R.layout.sqr__listview_container, null) as FrameLayout
        mRoot!!.addView(mContent)
        mListView.viewTreeObserver.addOnGlobalLayoutListener(mOnGlobalLayoutListener)
        val observer = ListViewScrollObserver(mListView)
        observer.setOnScrollUpAndDownListener(object : OnListViewScrollListener {
            override fun onScrollUpDownChanged(delta: Int, scrollPosition: Int, exact: Boolean) {
                onNewScroll(delta)
                snap(mHeaderTop == scrollPosition)
            }

            override fun onScrollIdle() {
                this@SimpleQuickReturn.onScrollIdle()
            }
        })
        if (mIsUsingHeader) {
            mRoot!!.addView(mRealHeader, mRealHeaderLayoutParams)
            mDummyHeader = View(mContext)
            val headerParams =
                AbsListView.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, mHeaderHeight)
            mDummyHeader!!.layoutParams = headerParams
            mListView.addHeaderView(mDummyHeader)
        }
        if (mIsUsingFooter) {
            mRoot!!.addView(mRealFooter, mRealFooterLayoutParams)
            mDummyFooter = View(mContext)
            val footerParams =
                AbsListView.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, mFooterHeight)
            mDummyFooter!!.layoutParams = footerParams
            mListView.addFooterView(mDummyFooter)
        }
    }

    private val mOnGlobalLayoutListener = OnGlobalLayoutListener {
        if (mIsWaitingForExactHeaderHeight) {
            mIsWaitingForExactHeaderHeight = false
            if (mIsUsingHeader) {
                if (mDummyHeader!!.height > 0) {
                    mHeaderHeight = mDummyHeader!!.height
                    val params = mDummyHeader!!.layoutParams as AbsListView.LayoutParams
                    params.height = mHeaderHeight
                    mDummyHeader!!.layoutParams = params
                }
            }
            if (mIsUsingFooter) {
                if (mDummyFooter!!.height > 0) {
                    mFooterHeight = mDummyFooter!!.height
                    val params = mDummyFooter!!.layoutParams as AbsListView.LayoutParams
                    params.height = mFooterHeight
                    mDummyFooter!!.layoutParams = params
                }
            }
        }
    }

    /**
     * Invoked when the user stops scrolling the content. In response we might start an animation to leave the header in
     * a fully open or fully closed state.
     */
    private fun onScrollIdle() {
        if (mIsSnapped) {
            // Only animate when header or footer is out of its natural position (truly over the content).
            return
        }
        if (mIsUsingHeader) {
            if (mHeaderTop > 0 || mHeaderTop <= -mHeaderHeight) {
                // Fully hidden, to need to animate.
                return
            }
        } else {
            if (mFooterBottom > 0 || mFooterBottom <= -mFooterHeight) {
                // Fully hidden, to need to animate.
                return
            }
        }
        if (mIsScrollingUp) {
            if (mIsUsingHeader && mIsUsingFooter) {
                animateHeaderFooter(
                    mHeaderTop.toFloat(),
                    -mHeaderHeight.toFloat(),
                    mFooterBottom.toFloat(),
                    -mFooterHeight.toFloat()
                )
            } else if (mIsUsingHeader) {
                animateHeaderFooter(mHeaderTop.toFloat(), -mHeaderHeight.toFloat(), 0f, 0f)
            } else if (mIsUsingFooter) {
                animateHeaderFooter(0f, 0f, mFooterBottom.toFloat(), -mFooterHeight.toFloat())
            }
        } else {
            if (mIsUsingHeader && mIsUsingFooter) {
                animateHeaderFooter(mHeaderTop.toFloat(), 0f, mFooterBottom.toFloat(), 0f)
            } else if (mIsUsingHeader) {
                animateHeaderFooter(mHeaderTop.toFloat(), 0f, 0f, 0f)
            } else if (mIsUsingFooter) {
                animateHeaderFooter(0f, 0f, mFooterBottom.toFloat(), 0f)
            }
        }
    }

    /**
     * Animates the marginTop property of the header between two specified values.
     * @param startTop Initial value for the marginTop property.
     * @param endTop End value for the marginTop property.
     */
    private fun animateHeaderFooter(
        startTop: Float,
        endTop: Float,
        startBottom: Float,
        endBottom: Float
    ) {
        cancelAnimation()
        val deltaTop = endTop - startTop
        val deltaBottom = endBottom - startBottom
        mAnimation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (mIsUsingHeader) {
                    mHeaderTop = (startTop + deltaTop * interpolatedTime).toInt()
                    mRealHeaderLayoutParams!!.topMargin = mHeaderTop
                    mRealHeader!!.layoutParams = mRealHeaderLayoutParams
                }
                if (mIsUsingFooter) {
                    mFooterBottom = (startBottom + deltaBottom * interpolatedTime).toInt()
                    mRealFooterLayoutParams!!.bottomMargin = mFooterBottom
                    mRealFooter!!.layoutParams = mRealFooterLayoutParams
                }
            }
        }
        var duration: Long = 0
        if (mIsUsingHeader) {
            duration = (deltaTop / mHeaderHeight.toFloat() * ANIMATION_DURATION).toLong()
            (mAnimation as Animation).setDuration(Math.abs(duration))
            mRealHeader!!.startAnimation(mAnimation)
            if (mIsUsingFooter) {
                mRealFooter!!.startAnimation(mAnimation)
            }
        } else {
            duration = (deltaBottom / mFooterHeight.toFloat() * ANIMATION_DURATION).toLong()
            (mAnimation as Animation).setDuration(Math.abs(duration))
            mRealFooter!!.startAnimation(mAnimation)
        }
    }

    private fun cancelAnimation() {
        if (mRealHeader != null) {
            mRealHeader!!.clearAnimation()
        }
        if (mRealFooter != null) {
            mRealFooter!!.clearAnimation()
        }
        mAnimation = null
    }

    private fun onNewScroll(delta: Int) {
        var delta = delta
        cancelAnimation()
        if (delta > 0) {
            if (mIsUsingHeader) {
                if (mHeaderTop + delta > 0) {
                    delta = -mHeaderTop
                }
            } else {
                if (mFooterBottom + delta > 0) {
                    delta = -mFooterBottom
                }
            }
        } else if (delta < 0) {
            if (mIsUsingHeader) {
                if (mHeaderTop + delta < -mHeaderHeight) {
                    delta = -(mHeaderHeight + mHeaderTop)
                }
            } else {
                if (mFooterBottom + delta < -mFooterHeight) {
                    delta = -(mFooterHeight + mFooterBottom)
                }
            }
        } else {
            return
        }
        mIsScrollingUp = delta < 0
        if (mIsUsingHeader) {
            mHeaderTop += delta
            if (mRealHeaderLayoutParams!!.topMargin != mHeaderTop) {
                mRealHeaderLayoutParams!!.topMargin = mHeaderTop
                mRealHeader!!.layoutParams = mRealHeaderLayoutParams
            }
        }
        if (mIsUsingFooter) {
            mFooterBottom += delta
            if (mRealFooterLayoutParams!!.bottomMargin != mFooterBottom) {
                mRealFooterLayoutParams!!.bottomMargin = mFooterBottom
                mRealFooter!!.layoutParams = mRealFooterLayoutParams
            }
        }
    }

    private fun snap(newValue: Boolean) {
        if (mIsSnapped == newValue) {
            return
        }
        mIsSnapped = newValue
        if (mOnSnappedChangeListener != null) {
            mOnSnappedChangeListener!!.onSnappedChange(mIsSnapped)
        }
    }

    companion object {
        protected const val TAG = "SimpleQuickReturn"

        /**
         * Maximum time it takes the show/hide animation to complete. Maximum because it will take much less time if the
         * header is already partially hidden or shown.
         *
         *
         * In milliseconds.
         */
        private const val ANIMATION_DURATION: Long = 400
    }
}