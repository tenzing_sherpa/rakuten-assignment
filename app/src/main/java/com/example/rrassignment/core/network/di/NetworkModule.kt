package com.example.rrassignment.core.network.di

import androidx.viewbinding.BuildConfig
import com.example.rrassignment.photos.data.api.PhotosApi
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton


/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    /**
     * provide photoApiService for fetching data from remote server
     * @param retrofit
     * */
    @Provides
    @Singleton
    fun providePhotosApiService(retrofit: Retrofit): PhotosApi =
        retrofit.create(PhotosApi::class.java)


    /**
     * provider for building Retrofit Builder with httpClient, converter factory
     * @param httpClient OKHttpClient Builder
     * @param convertFactory GsonConverterFactory
     * */
    @Singleton
    @Provides
    fun provideGsonRetrofit(
        httpClient: OkHttpClient.Builder,
    convertFactory: GsonConverterFactory
    ) : Retrofit = Retrofit.Builder()
        .baseUrl("http://yuriy.me/rakuten-rewards/")
        .client(httpClient.build())
        .addConverterFactory(convertFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()


    /**
     * provider for building OkHttp with interceptor
     * @param httpLoggingInterceptor HttpLoggingInterceptor
     * */
    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient.Builder {
        val httpClient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(httpLoggingInterceptor)
        }
        httpClient.retryOnConnectionFailure(true)
        return httpClient

    }

    /**
     * provider for building HttpLoggingInterceptor
     * */
    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)


    /**
     * provider for building JacksonConverterFactory
     * */
    @Provides
    @Singleton
    fun provideJacksonConverterFactory(): JacksonConverterFactory =
        JacksonConverterFactory.create()
    /**
     * provider for building GsonConverterFactory
     * */
    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create()
}