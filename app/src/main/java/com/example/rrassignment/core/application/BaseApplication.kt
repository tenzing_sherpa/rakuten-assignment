package com.example.rrassignment.core.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
@HiltAndroidApp
class BaseApplication: Application(){
}