package com.example.rrassignment.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rrassignment.photos.data.database.PhotosDao
import com.example.rrassignment.photos.domain.model.PhotosData

/**
 * Created by TENZING SHERPA on 4/2/22.
 * Email: tenzingherpaaa@gmail.com
 */
@Database(entities = [PhotosData::class], version = 5, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun photosDao(): PhotosDao
}