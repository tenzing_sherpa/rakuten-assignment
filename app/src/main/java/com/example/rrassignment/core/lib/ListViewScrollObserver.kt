package com.example.rrassignment.core.lib

import android.widget.AbsListView
import android.widget.ListView

/**
 * Created by TENZING SHERPA on 4/3/22.
 * Email: tenzingherpaaa@gmail.com
 */
class ListViewScrollObserver(listView: ListView) : AbsListView.OnScrollListener {
    private var mListViewScrollListener: OnListViewScrollListener? = null
    private var mLastFirstVisibleItem = 0
    private var mLastTop = 0
    private var mScrollPosition = 0
    private var mLastHeight = 0

    interface OnListViewScrollListener {
        fun onScrollUpDownChanged(delta: Int, scrollPosition: Int, exact: Boolean)
        fun onScrollIdle()
    }

    fun setOnScrollUpAndDownListener(listener: OnListViewScrollListener?) {
        mListViewScrollListener = listener
    }

    override fun onScroll(
        view: AbsListView,
        firstVisibleItem: Int,
        visibleItemCount: Int,
        totalItemCount: Int
    ) {
        val firstChild = view.getChildAt(0) ?: return
        val top = firstChild.top
        val height = firstChild.height
        val delta: Int
        var skipped = 0
        when {
            mLastFirstVisibleItem == firstVisibleItem -> {
                delta = mLastTop - top
            }
            firstVisibleItem > mLastFirstVisibleItem -> {
                skipped = firstVisibleItem - mLastFirstVisibleItem - 1
                delta = skipped * height + mLastHeight + mLastTop - top
            }
            else -> {
                skipped = mLastFirstVisibleItem - firstVisibleItem - 1
                delta = skipped * -height + mLastTop - (height + top)
            }
        }
        val exact = skipped > 0
        mScrollPosition += -delta
        if (mListViewScrollListener != null) {
            mListViewScrollListener!!.onScrollUpDownChanged(-delta, mScrollPosition, exact)
        }
        mLastFirstVisibleItem = firstVisibleItem
        mLastTop = top
        mLastHeight = firstChild.height
    }

    override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {
        if (mListViewScrollListener != null && scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
            mListViewScrollListener!!.onScrollIdle()
        }
    }

    init {
        listView.setOnScrollListener(this)
    }
}